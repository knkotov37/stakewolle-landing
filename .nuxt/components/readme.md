# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<AboutUs>` | `<about-us>` (components/AboutUs.vue)
- `<Bar>` | `<bar>` (components/Bar.vue)
- `<BarGraph>` | `<bar-graph>` (components/BarGraph.vue)
- `<Benefits>` | `<benefits>` (components/Benefits.vue)
- `<CalcValidator>` | `<calc-validator>` (components/CalcValidator.vue)
- `<Calculator>` | `<calculator>` (components/Calculator.vue)
- `<Delegates>` | `<delegates>` (components/Delegates.vue)
- `<Footer>` | `<footer>` (components/Footer.vue)
- `<LineChart>` | `<line-chart>` (components/LineChart.vue)
- `<LinkUs>` | `<link-us>` (components/LinkUs.vue)
- `<Navigation>` | `<navigation>` (components/Navigation.vue)
- `<Nets>` | `<nets>` (components/Nets.vue)
- `<SoonValidators>` | `<soon-validators>` (components/SoonValidators.vue)
- `<Stages>` | `<stages>` (components/Stages.vue)
- `<StartBlock>` | `<start-block>` (components/StartBlock.vue)
- `<TargetBlock>` | `<target-block>` (components/TargetBlock.vue)
- `<MobileAboutUs>` | `<mobile-about-us>` (components/mobile/mobileAboutUs.vue)
- `<MobileBenefits>` | `<mobile-benefits>` (components/mobile/mobileBenefits.vue)
- `<MobileCalculator>` | `<mobile-calculator>` (components/mobile/mobileCalculator.vue)
- `<MobileCall>` | `<mobile-call>` (components/mobile/mobileCall.vue)
- `<MobileDelegates>` | `<mobile-delegates>` (components/mobile/mobileDelegates.vue)
- `<MobileFooter>` | `<mobile-footer>` (components/mobile/mobileFooter.vue)
- `<MobileNavigation>` | `<mobile-navigation>` (components/mobile/mobileNavigation.vue)
- `<MobileStacking>` | `<mobile-stacking>` (components/mobile/mobileStacking.vue)
- `<MobileStartBlock>` | `<mobile-start-block>` (components/mobile/mobileStartBlock.vue)
- `<MobileStonks>` | `<mobile-stonks>` (components/mobile/mobileStonks.vue)
- `<MobileValidators>` | `<mobile-validators>` (components/mobile/mobileValidators.vue)
- `<MobileYears>` | `<mobile-years>` (components/mobile/mobileYears.vue)
