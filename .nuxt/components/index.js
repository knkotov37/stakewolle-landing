export { default as AboutUs } from '../../components/AboutUs.vue'
export { default as Bar } from '../../components/Bar.vue'
export { default as BarGraph } from '../../components/BarGraph.vue'
export { default as Benefits } from '../../components/Benefits.vue'
export { default as CalcValidator } from '../../components/CalcValidator.vue'
export { default as Calculator } from '../../components/Calculator.vue'
export { default as Delegates } from '../../components/Delegates.vue'
export { default as Footer } from '../../components/Footer.vue'
export { default as LineChart } from '../../components/LineChart.vue'
export { default as LinkUs } from '../../components/LinkUs.vue'
export { default as Navigation } from '../../components/Navigation.vue'
export { default as Nets } from '../../components/Nets.vue'
export { default as SoonValidators } from '../../components/SoonValidators.vue'
export { default as Stages } from '../../components/Stages.vue'
export { default as StartBlock } from '../../components/StartBlock.vue'
export { default as TargetBlock } from '../../components/TargetBlock.vue'
export { default as MobileAboutUs } from '../../components/mobile/mobileAboutUs.vue'
export { default as MobileBenefits } from '../../components/mobile/mobileBenefits.vue'
export { default as MobileCalculator } from '../../components/mobile/mobileCalculator.vue'
export { default as MobileCall } from '../../components/mobile/mobileCall.vue'
export { default as MobileDelegates } from '../../components/mobile/mobileDelegates.vue'
export { default as MobileFooter } from '../../components/mobile/mobileFooter.vue'
export { default as MobileNavigation } from '../../components/mobile/mobileNavigation.vue'
export { default as MobileStacking } from '../../components/mobile/mobileStacking.vue'
export { default as MobileStartBlock } from '../../components/mobile/mobileStartBlock.vue'
export { default as MobileStonks } from '../../components/mobile/mobileStonks.vue'
export { default as MobileValidators } from '../../components/mobile/mobileValidators.vue'
export { default as MobileYears } from '../../components/mobile/mobileYears.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
